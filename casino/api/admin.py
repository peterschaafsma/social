from django.contrib import admin
from api.models import *
from django import forms


# Register your models here.
#admin.site.register(Stock)

@admin.register(Stock)
class StockAdmin(admin.ModelAdmin):
    list_display = ('symbol', 'last_seen')

@admin.register(TwitterAuthor)
class TwitterAuthorAdmin(admin.ModelAdmin):
    list_display = ('screen_name', 'priority')

@admin.register(StockPrice)
class StockPriceAdmin(admin.ModelAdmin):
    list_display = ('symbol', 'last_price', 'last_update')

@admin.register(Config)
class ConfigAdmin(admin.ModelAdmin):
    list_display = ('tag', 'json')

class ArticleSourceModelForm(forms.ModelForm):
    data = forms.CharField(required=False, widget=forms.Textarea)
    last_error = forms.CharField(required=False, widget=forms.Textarea)
    class Meta:
        model = ArticleSource
        fields = '__all__'

@admin.register(ArticleSource)
class ArticleSourceAdmin(admin.ModelAdmin):
    form = ArticleSourceModelForm
    list_display = ('symbol', 'url', 'enabled', 'error_count', '_next_scrape', '_last_success', '_last_new_article', 'scrape_count', 'article_count', 'type', 'status', 'scrape_time')

@admin.register(Article)
class StockPriceAdmin(admin.ModelAdmin):
    list_display = ('source', 'title', '_created')

