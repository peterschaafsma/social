from api.models import Config
from channels.generic.websocket import WebsocketConsumer


import hashlib
import json
import re
import requests


class TwitterApi:
    def __init__(self):
        self.rules_url = 'https://api.twitter.com/2/tweets/search/stream/rules'
        self.headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer AAAAAAAAAAAAAAAAAAAAADOa8QAAAAAAJFmp2%2BRt94B7LwkA3hCEhrJNrik%3Dlg6D0G9k2kDEM2lARDG2sbbYUTfxy3eyKwIZUecHeuSg5RxZrI"
        }

        self.symbols = set()
        self.rule_ids = []

        self.init_symbols()

    def init_symbols(self):
        response = requests.get(url=self.rules_url, headers=self.headers)
        if response.status_code != 200:
            print(response.content)
            raise Exception("COULD NOT GET RULES")

        content = json.loads(response.content)
        rules = [rule for rule in content['data'] if rule['tag'].startswith('__symbols')]

        self.rule_ids = [rule['id'] for rule in rules]
        self.symbols = set(symbol for rule in rules for symbol in re.findall(r'(?<=\$)\w+', rule['value']))

        print("current rule IDs and symbols", self.rule_ids, self.symbols)

    def update_symbols(self, action):
        return  # temporarily disabled

        if action == 'set':
            configs = [value['config'] for value in Consumer.CONNECTIONS.values()]
            symbols = set(symbol for config in configs for symbol in config.get('symbols', []))

            if symbols == self.symbols:
                print('No change in symbols')
                return

            symbols = sorted(symbols)

        value_format = '({}) lang:en -is:quote -is:retweet -is:reply'

        size = 28
        count = 0
        rules = []
        for i in range(0, len(symbols), size):
            count += 1
            batch = symbols[i:i+28]
            batch = map(lambda x: x.strip('$'), batch)
            expression = ' OR '.join(map(lambda x: '"${}"'.format(x.strip('$')), batch))

            rule = {'value': value_format.format(expression), 'tag': '__symbols_{}'.format(count)}
            rules.append(rule)

        # delete current IDs
        if self.rule_ids:
            data = {"delete": {"ids": self.rule_ids}}
            # print("delete rules data", data)
            # return

            response = requests.post(self.rules_url, headers=self.headers, json=data)
            if response.status_code != 200:
                print(response.content)
                raise Exception("COULD NOT DELETE RULES")

        # add new rules
        if rules:
            data = {"add": rules}
            # print("add rules data", data)
            # return

            response = requests.post(self.rules_url, headers=self.headers, json=data)
            if response.status_code != 201:
                print(response.content)
                raise Exception("COULD NOT ADD RULES")

            print("SUCCESSFULLY TRACKING SYMBOLS", symbols)


class Consumer(WebsocketConsumer):
    CONNECTIONS = {}
    TWITTER = None

    def _register(self, data):
        print("register", data)
        config = data['config']
        config_hash = hashlib.md5(json.dumps(config, sort_keys=True).encode('utf-8')).hexdigest()

        if config_hash not in Consumer.CONNECTIONS:
            Consumer.CONNECTIONS[config_hash] = {
                'config': config,
                'sockets': set()
            }

        Consumer.CONNECTIONS[config_hash]['sockets'].add(self)

        if self.TWITTER:
            self.TWITTER.update_symbols('set')

        params = {}
        for param in ('symbols', 'type', 'size'):
            if param not in config:
                params = None
                break

            if param == 'symbols':
                params['symbols'] = ','.join(config['symbols'])
            else:
                params[param] = config[param]

        if params:
            config, created = Config.objects.get_or_create(tag='default')
            config.json = json.dumps(params)
            config.save()

    def _unregister(self):
        delete = []
        for config_hash in Consumer.CONNECTIONS:
            if self in Consumer.CONNECTIONS[config_hash]['sockets']:
                Consumer.CONNECTIONS[config_hash]['sockets'].remove(self)

                if len(Consumer.CONNECTIONS[config_hash]['sockets']) == 0:
                    delete.append(config_hash)

        for config_hash in delete:
            del Consumer.CONNECTIONS[config_hash]

    def connect(self):
        self.accept()

        if not self.TWITTER:
            self.TWITTER = TwitterApi()

        print('CONNECT')

    def disconnect(self, close_code):
        print('DISCONNECT', close_code)

        self._unregister()

        print(Consumer.CONNECTIONS)

    def receive(self, text_data):
        print('RECEIVE', text_data)

        data = json.loads(text_data)
        if data['action'] == 'register':
            self._register(data)

        elif data['action'] == 'unregister':
            self._unregister()

        print(Consumer.CONNECTIONS)

    @staticmethod
    def send_all(data):
        for value in Consumer.CONNECTIONS.values():
            for socket in value['sockets']:
                super(Consumer, socket).send(json.dumps(data))

    @staticmethod
    def send(config_hash, data):
        if config_hash in Consumer.CONNECTIONS:
            for socket in Consumer.CONNECTIONS[config_hash]['sockets']:
                # print("SEND {} to {}".format(config_hash, socket))
                super(Consumer, socket).send(json.dumps(data))