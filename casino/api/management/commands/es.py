from django.core.management.base import BaseCommand
from tools.queues import dequeue

import elasticsearch
import elasticsearch.helpers
import json
import sys


SETTINGS = {
    # 'analysis': {
    #     'normalizer': {
    #         'lowercase': {
    #             'type': 'custom',
    #             'char_filter': [],
    #             'filter': ['lowercase']
    #         },
    #         'keyword': {
    #             'type': 'custom',
    #             'char_filter': [],
    #             'filter': ['lowercase', 'asciifolding']
    #         }
    #     },
    #     'analyzer': {
    #         'character': {
    #             'type': 'pattern',
    #             'pattern': '(?=.)',
    #             'lowercase': False
    #         },
    #         'word': {
    #             'type': 'custom',
    #             'tokenizer': 'letter',
    #             'filter': ['lowercase', 'asciifolding']
    #         },
    #     }
    # },
    'index': {
        'number_of_shards': 1,
        'number_of_replicas': 0,
    }
}

CONFIG = {
    'posts': {
        'mappings': {
            'post': {
                'dynamic': True,
            }
        },
        'settings': SETTINGS
    },
}

class ElasticSearch():
    def __init__(self):
        self.client = elasticsearch.Elasticsearch(hosts=['http://localhost:9200'], timeout=30)

    def create_index(self, index, body):
        print(index)
        print(body)
        elasticsearch.client.IndicesClient(self.client).create(index=index, body=body)

    def delete_index(self, index):
        elasticsearch.client.IndicesClient(self.client).delete(index=index)

    def index(self, statuses):
        actions = []
        for status in statuses:
            actions.append({
                '_index': 'posts',
                '_id': status['id'],
                '_type': 'post',
                '_source': status
            })

        try:
            elasticsearch.helpers.bulk(self.client, actions)
        except:
            print("actions")
            print(actions)
            raise


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, *kwargs)

        self.es = ElasticSearch()

    def _parse_args(self, *args):
        options = {}
        for arg in args:
            key, value = arg.split('=', 1)
            key = key.replace('-', '_')

            options[key] = value

        return options

    def add_arguments(self, parser):
        parser.add_argument('--create-index', nargs='*')
        parser.add_argument('--delete-index', nargs='*')
        parser.add_argument('--process-queue', nargs='*')

    def _process_queue(self):
        print("Processing ES indexing queue")
        def callback(channel, method, properties, body):
            sys.stdout.write('i')
            sys.stdout.flush()
            status = json.loads(body.decode())

            blacklist = ['symbol_opacity']
            for field in blacklist:
                if field in status:
                    del status[field]

            self.es.index([status])

        dequeue('index', callback)

    def handle(self, *args, **kwargs):
        if kwargs['create_index'] is not None:
            options = self._parse_args(*kwargs['create_index'])

            self.es.create_index(options['index'], CONFIG[options['config']])

        elif kwargs['delete_index'] is not None:
            options = self._parse_args(*kwargs['delete_index'])

            self.es.delete_index(options['index'])

        elif kwargs['process_queue'] is not None:
            self._process_queue()
