from core.posthandler import PostHandler
from django.core.management.base import BaseCommand
from tools.queues import dequeue

import json
import requests
import sys


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, *kwargs)

    def add_arguments(self, parser):
        # parser.add_argument('--process-queue', nargs='*')
        pass

    def handle(self, *args, **kwargs):
        print("Processing post queue to Django")
        def callback(channel, method, properties, body):
            url = 'http://localhost:8000/site/posts'
            # print("posting", data)
            sys.stdout.write('p')
            sys.stdout.flush()
            try:
                data = json.loads(body.decode())

                response = requests.post(url, json=data)
                if response.status_code != 200:
                    print(response.content)
            except Exception as e:
                print("DROPPING POST DUE TO {}".format(e))

        dequeue('posts', callback)