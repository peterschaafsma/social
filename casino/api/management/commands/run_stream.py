from datetime import datetime, timedelta, timezone
from dateutil.parser import parse
from django.core.management.base import BaseCommand
from prawcore.exceptions import NotFound
from requests.exceptions import ChunkedEncodingError
from tools.queues import enqueue

import elasticsearch.helpers
import json
import praw
import re
import sys
import time
import threading
import traceback
import requests


es = elasticsearch.Elasticsearch(hosts=['http://localhost:9200'], timeout=30)


class Command(BaseCommand):
    def __init__(self):
        super().__init__(self)

        self.stop_processing = False

    def handle(self, *args, **kwargs):
        actions = kwargs['action'].split(',')

        tthread = None
        rthread = None
        if 'twitter' in actions:
            tthread = threading.Thread(target=self.handle_twitter_stream)
            tthread.start()

        if 'reddit' in actions:
            rthread = threading.Thread(target=self.handle_reddit_stream)
            rthread.start()

        try:
            if tthread:
                tthread.join()
            if rthread:
                rthread.join()
            # dthread.join()
            # fthread.join()

        except KeyboardInterrupt:
            print("STOPPING")
            self.stop_processing = True


    def add_arguments(self, parser):
        parser.add_argument('--action', type=str)

    def post(self, source, post):
        data = {
            'source': source,
            'posts': [post]
        }

        # url = 'http://localhost:8000/site/posts'
        # # print("posting", data)
        # try:
        #     response = requests.post(url, json=data)
        #     if response.status_code != 200:
        #         print(response.content)
        # except Exception as e:
        #     print("DROPPING POST DUE TO {}".format(e))

        enqueue(data, 'posts')

    def handle_twitter_stream(self):
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer AAAAAAAAAAAAAAAAAAAAADOa8QAAAAAAJFmp2%2BRt94B7LwkA3hCEhrJNrik%3Dlg6D0G9k2kDEM2lARDG2sbbYUTfxy3eyKwIZUecHeuSg5RxZrI"
        }

        while True:
            try:
                print("New stream")
                request = requests.get('https://api.twitter.com/2/tweets/search/stream?expansions=author_id&tweet.fields=context_annotations,created_at,entities,id,text&user.fields=id,name,username,public_metrics', headers=headers, stream=True)

                heartbeat_time = time.time()

                print("Start reading")
                for data in request.iter_lines():
                    if self.stop_processing:
                        break

                    if not data:
                        now = time.time()

                        heartbeat = int(now - heartbeat_time)
                        heartbeat_time = now

                        if heartbeat > 30:
                            self.stop_processing = True
                            break

                        print("Heartbeat: {}s".format(heartbeat))

                        continue

                    # print("data", data)

                    status = json.loads(data.decode('utf-8'))
                    post = self.get_twitter_post(status)

                    self.post('twitter', post)

            except ChunkedEncodingError as e:
                print("ChunkedEncodingError")
                stacktrace = traceback.format_exc()

                print(stacktrace)

            except Exception as e:
                print("General Exception. Quitting.")
                print(e)
                stacktrace = traceback.format_exc()

                print(stacktrace)

                self.stop_processing = True

                break

            print("Sleeping for 10 seconds before trying again")
            time.sleep(10)


    def get_followed_symbols(self):
        try:
            response = requests.get('http://localhost:8000/site/symbols')

            data = json.loads(response.content)

            return data['symbols']
        except Exception as e:
            raise e

    def handle_reddit_stream(self):
        reddit = praw.Reddit(client_id='T_iztRgrzX18EQ', client_secret='-YSFgzZ023wqp85UsFcvzCDyHuFMNQ',
                             user_agent='cmd', user_name='dee_em_me_side_boob', password='Dexter007')

        default_subreddits = [
            'stocks',
            'wallstreetbets',
            'investing',
            'stockmarket',
            'options',
            'shortsqueeze'
        ]

        def subreddit_exists(name):
            exists = True
            try:
                reddit.subreddits.search_by_name(name, exact=True)
            except NotFound:
                exists = False
            return exists

        def submissions():
            while True:
                followed_symbols = self.get_followed_symbols()

                symbols = filter(subreddit_exists, followed_symbols)
                symbolreddits = [symbol.lower() for symbol in symbols]
                subreddits = symbolreddits + default_subreddits

                for submission in reddit.subreddit("+".join(subreddits)).stream.submissions():
                    if self.stop_processing:
                        break

                    new_followed_symbols = self.get_followed_symbols()
                    if set(new_followed_symbols) != set(followed_symbols):
                        break

                    if not submission.selftext:
                        continue

                    post = self.get_reddit_post(submission, 'submission')

                    self.post('reddit', post)

                if self.stop_processing:
                    break

        # def comments():
        #     for comment in reddit.subreddit("+".join(subreddits)).stream.comments():
        #         if self.stop_processing:
        #             break
        #
        #         if not comment.body:
        #             continue
        #
        #         self.listener.on_reddit_status(comment, 'comment')

        # cthread = threading.Thread(target=comments)
        # cthread.start()

        sthread = threading.Thread(target=submissions)
        sthread.start()

        # cthread.join()
        sthread.join()

    def get_twitter_post(self, status):
        sys.stdout.write('.')
        sys.stdout.flush()

        def get(path, default=None):
            try:
                data = status
                for element in path.split('/'):
                    data = data[element]
                return data
            except:
                return default

        user_map = {user['id']: user for user in get('includes/users', {})}

        try:
            author = user_map[get('data/author_id')]
        except:
            print(status)
            raise

        post = dict()
        post['source'] = 'twitter'
        post['type'] = 'tweet'
        post['author'] = author['username']
        post['author_url'] = 'https://twitter.com/{}'.format(post['author'])
        post['followers_count'] = author['public_metrics']['followers_count']
        post['created_at'] = parse(get('data/created_at')).strftime('%Y-%m-%dT%H:%M:%S.%f%z')
        post['received_at'] = datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%f%z')
        post['id'] = get('data/id')
        post['post_url'] = 'https://twitter.com/{}/status/{}'.format(post['author'], post['id'])

        post['tags'] = (rule['tag'] for rule in get('matching_rules', []))
        post['tags'] = (re.sub(r'__symbols.*', 'stock', tag) for tag in post['tags'])
        post['tags'] = (re.sub(r'__vip.*', 'vip',  tag) for tag in post['tags'])
        post['tags'] = list(set(post['tags']))

        post['full_text'] = get('data/text')
        post['text'] = post['full_text']

        # print(post['text'], [rule['tag'] for rule in status['matching_rules']], post['tags'])

        return post

    def get_reddit_post(self, status, status_type):
        post = {}

        if status_type == 'submission':
            sys.stdout.write(',')
            sys.stdout.flush()

            post['title'] = status.title
            post['full_text'] = status.selftext
            post['full_html'] = status.selftext_html

            post['author'] = status.author.name
            post['author_url'] = 'https://reddit.com/{}'.format(status.author._path)

            post['created_at'] = datetime.utcfromtimestamp(status.created_utc).strftime('%Y-%m-%dT%H:%M:%S.%f%z')
            post['received_at'] = datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%f%z')
            post['post_url'] = 'https://reddit.com{}'.format(status.permalink)

            post['id'] = status.id
            post['source'] = 'reddit'
            post['type'] = 'submission'

            post['tags'] = [status.subreddit.display_name]

        return post

