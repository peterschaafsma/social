from api.models import Article, ArticleSource
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timedelta, timezone
from django.db.models import Q
from django.core.management.base import BaseCommand
from tools.queues import enqueue

import bs4
import json
import os
import praw
import re
import requests
import subprocess
import threading
import time
import traceback
import tweepy


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        if kwargs['reset_all']:
            return self.reset_all()

        if kwargs['test']:
            return self.test(**kwargs)

        if kwargs['mode'] == 'loop':
            self.handle_loop(**kwargs)
        elif kwargs['mode'] == 'add':
            self.handle_add(**kwargs)
        elif kwargs['mode'] == 'single':
            self.handle_single(**kwargs)

    def test(self, **kwargs):
        symbols = map(str.lower, kwargs['test'].split(','))

        for symbol in symbols:
            print("Testing {}".format(symbol))
            sources = list(ArticleSource.objects.filter(symbol__iexact=symbol))
            if not sources:
                print("No source found for symbol {}".format(symbol))

            elif len(sources) > 1:
                print("Multiple sources found for {}: {}", format(symbol, [s.symbol for s in sources]))

            else:
                source = sources[0]
                print("Found 1 source for \"{}\"".format(symbol, source.symbol))

                articles = list(Article.objects.filter(source=source).order_by('-_created'))
                if not articles:
                    print("No articles found for source {}", symbol)
                else:
                    print("Found {} articles for {}".format(len(articles), symbol))
                    article = articles[0]
                    print('Last article entered at {}: "{}"'.format(article._created.strftime("%Y-%m-%d"), article.title))

    def reset_all(self):
        for source in ArticleSource.objects.filter(enabled=False):
            print("enabling", source.symbol)
            source.enabled = True
            source.save()

    def handle_single(self, **kwargs):
        kwargs['type'] = 'http'
        if kwargs['type'] == 'http':
            url = kwargs.get('url') or 'https://group.jumia.com/news/press-releases/'
            text = kwargs.get('text') or 'UNFPA partners with Jumia for'

            result, status = self._handle_http_base(url, text)
        elif kwargs['type'] == 'curl':
            curl = '''
curl 'https://ir.amerisbank.com/feed/PressRelease.svc/GetPressReleaseList?apiKey=BF185719B0464B3CB809D23926182246&LanguageId=1&bodyType=3&pressReleaseDateFilter=3&categoryId=1cb807d2-208f-4bc3-9133-6a9ad45ac3b0&pageSize=10&pageNumber=0&tagList=&includeTags=true&year=-1&excludeSelection=1' \
  -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
  -H 'X-NewRelic-ID: VQYBUlRVChACVlhbBQMCVlU=' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36' \
  -H 'Content-Type: application/json; charset=utf-8' \
  -H 'Accept: application/json, text/javascript, */*; q=0.01' \
  -H 'Referer: https://ir.amerisbank.com/news-market-data/news/default.aspx' \
  -H 'X-Requested-With: XMLHttpRequest' \
  --compressed            
  '''

            text = 'AMERIS BANCORP ANNOUNCES FOURTH QUARTER AND FULL YEAR 2020 FINANCIAL RESULTS'

            result, status = self._handle_curl_base(curl, text)

        print(status)
        if status == 'ok':
            for title in result:
                print(title)

    def handle_loop(self, **kwargs):
        def handle_thread(s):
            if s.type == 'http':
                return self._handle_http(s)
            elif s.type == 'curl':
                return self._handle_curl(s)

        while True:
            self.now = datetime.now(timezone.utc)

            if kwargs['symbol']:
                sources = ArticleSource.objects.filter(Q(symbol__iexact=kwargs['symbol']))
            else:
                sources = ArticleSource.objects.filter(Q(enabled=True) & (Q(_next_scrape__isnull=True) | Q(_next_scrape__lt=self.now)))

            sources = list(sources)

            if sources:
                print("Scraping {} sources".format(len(sources)))

                print("Pool's open!")
                with ThreadPoolExecutor(5) as executor:
                    result = executor.map(handle_thread, sources)
                print(list(result))

            article = ArticleSource.objects.filter(_next_scrape__isnull=False).order_by('-_next_scrape')[0]
            sleep = max(1, int((article._next_scrape - self.now).total_seconds() + 1.0))

            print("Status ok: {}/{}".format(ArticleSource.objects.filter(status='ok').count(), ArticleSource.objects.all().count()))

            if sleep:
                print("Nothing to do. Sleeping until around {}".format(self.now + timedelta(seconds=sleep)))
                time.sleep(sleep)

            print()

    def add_arguments(self, parser):
        parser.add_argument('--mode', type=str, default='loop')
        parser.add_argument('--text', type=str)
        parser.add_argument('--source', type=str)
        parser.add_argument('--url', type=str)
        parser.add_argument('--type', type=str, default='http')
        parser.add_argument('--test', type=str)
        parser.add_argument('--symbol', type=str)
        parser.add_argument('--reset-all', action="store_true")
        parser.add_argument('--dry-run', action="store_true")

    def _have_titles(self, source, titles):
        existing_articles = Article.objects.filter(source=source, title__in=titles)
        existing_titles = [article.title for article in existing_articles]

        new_titles = set(titles) - set(existing_titles)
        for title in new_titles:
            title = title[:1023]
            article = Article.objects.create(source=source, title=title)

            source._last_new_article = self.now

            def f(article):
                #os.system("/usr/bin/canberra-gtk-play --id='service-login'")
                now = datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S.%f%z')
                post = {
                    'text': article.title,
                    'html': article.title,
                    'tags': ['press'],
                    'author': article.source.symbol,
                    'author_url': article.source.url,
                    'created_at': now,
                    'post_url': article.source.url,
                    'symbols': [article.source.symbol],
                    'source': 'press',
                    'id': article.id,
                    'ranking': {},
                    'type': 'article',
                    'received_at': now,
                }

                print("posting press release", post)

                data = {
                    'source': 'press',
                    'posts': [post]
                }
                # url = 'http://localhost:8000/site/posts'
                # requests.post(url, json=data)

                enqueue(data, 'posts')

            thread = threading.Thread(target=f, args=(article, ))
            thread.start()

            print("{} on {}".format(title, source.url))

        return new_titles

    def _handle_curl_base(self, curl, text):
        def search(d, v):
            if type(d) is list:
                # print("list", len(d))
                for i, _ in enumerate(d):
                    r = search(d[i], v)
                    if r:
                        return [('list', i)] + r
                return None
            elif type(d) is dict:
                # print("dict", list(d.keys()))
                for k in d:
                    r = search(d[k], v)
                    if r:
                        return [('dict', k)] + r
                else:
                    return None
            else:  # is value
                # print("value", d)
                # if v.startswith('The Government of Ontario, Canadian Manufacturers'):
                #     if type(d) is str and d.startswith('The Government of Ontario, Canadian Manufacturers'):
                #         print(v, d, v.lower() in d.lower())
                if type(d) is str and d.lower().startswith(v.lower()):
                    return [('value', None)]
                else:
                    return None

        def get_all(d, p):
            # print("get_all", d, p)
            t, v = p[0]
            r = []
            if t == 'value':
                r.append(d)
            elif t == 'list':
                for i, _ in enumerate(d):
                    r.extend(get_all(d[i], p[1:]))
            elif t == 'dict':
                if v in d:
                    r.extend(get_all(d[v], p[1:]))
            return r

        curl = re.sub(r'[\\\n\r]', ' ', curl)
        curl = re.sub(r'curl ', r'curl -s ', curl)

        status, output = subprocess.getstatusoutput(curl)
        if status == 0:
            try:
                if not output:
                    return output, 'empty response'

                data = json.loads(output)

                path = search(data, text)

                if not path:
                    print(curl)
                    print(output)
                    return output, 'cannot find text'

                titles = get_all(data, path)

                return titles, 'ok'

            except:
                stacktrace = traceback.format_exc()

                print(stacktrace)

                return stacktrace, 'exception'

        else:
            return output, 'curl error'

    def _handle_curl(self, source):
        scrape_start = time.time()

        articles = list(Article.objects.filter(source=source).order_by('-_created'))
        if not articles:
            result, status = '', 'no article'
        else:
            for index, article in enumerate(articles):
                text = article.title

                result = None
                status = None
                for mode in ['as_is', 'new_token']:
                    if mode == 'as_is':
                        result, status = self._handle_curl_base(source.data, text)

                        if status == 'ok':
                            break

                        if status != 'cannot find text':
                            break

                    elif mode == 'new_token':
                        if not re.search(r'getHeadlines.*token=', source.data):
                            return result, status

                        cmd = '/home/peter/programming/stock/casino/tools/gettoken.py {}'.format(source.url)
                        exit_code, new_token = subprocess.getstatusoutput(cmd)

                        if exit_code or not new_token:
                            status = 'cannot obtain new token'
                            break

                        curl = source.data
                        tokens = re.findall(r'(?<=token=)\w+', source.data)
                        for token in tokens:
                            curl = re.sub(token, new_token, curl)

                        source.data = curl
                        source.save()

                        result, status = self._handle_curl_base(source.data, text)

                        if status == 'ok':
                            print("new token successfully obtained for {}!".format(source.symbol))
                            break

                        if status != 'new token was unsuccessful':
                            break

                if status == 'ok':
                    break

                if index > 10:
                    break

        if status == 'ok':
            new_titles = self._have_titles(source, result)

            source.last_error = None
            source.error_count = 0

            source.scrape_count += 1
            source.article_count += len(new_titles)

            source.scrape_time = time.time() - scrape_start
            source._last_success = self.now
            source._scraped = self.now

        else:
            source.last_error = result
            source.error_count += 1

        if source.error_count > 20:
            source.enabled = False

        source._next_scrape = self.now + timedelta(seconds=60)
        source.status = status
        source.save()

        return status

    def _handle_http_base(self, url, text):
        try:
            soup = None
            hit = None
            for fetch in ['http', 'curl']:
                if fetch == 'http':
                    # print("TRY AS HTTP")
                    soup = bs4.BeautifulSoup(requests.get(url).content.decode('utf-8'), features='lxml')
                elif fetch == 'curl':
                    # print("TRY AS CURL")
                    curl = "curl '{}'".format(url)
                    status, output = subprocess.getstatusoutput(curl)
                    if status == 0:
                        soup = bs4.BeautifulSoup(output, features='lxml')
                    else:
                        continue

                if not soup:
                    continue

                for search in ['verbatim', 'regex', 'longest_text']:
                    if search == 'verbatim':
                        hit = soup.find(text=text)
                    elif search == 'regex':
                        hit = soup.find(text=re.compile(r'.*{}.*'.format(text), re.IGNORECASE))
                    elif search == 'longest_text':
                        longest_texty_text = max(re.findall('r[a-zA-Z0-9 \.\,]+', text), key=lambda x: len(x))
                        hit = soup.find(text=re.compile(r'.*{}.*'.format(longest_texty_text), re.IGNORECASE))

                    if hit:
                        break

                if hit:
                    break

            if not soup:
                return output, 'cannot make soup'

            if not hit:
                return None, 'cannot find text in soup'

            parents = [p.name for p in hit.parents][::-1]
            classes = [p.get('class') for p in hit.parents][::-1]

            # print("CLASSES", classes)

            def filter_illegal_classes(class_list):
                if class_list is None:
                    return None
                class_list = list(class_list)
                for index, class_name in enumerate(class_list):
                    if re.match(r'^.*\d{3,}$', class_name):
                        class_list[index] = None
                    if re.match(r'^(col|tag|has|category)[-].*$', class_name):
                        # print("drop {}".format(class_name))
                        class_list[index] = None
                new_class_list = list(filter(None, class_list))
                if new_class_list:
                    return new_class_list
                else:
                    return None

            classes = list(map(filter_illegal_classes, classes))
            # print("classes", classes)

            all_elements = [soup]
            for tag, class_ in zip(parents[1:], classes[1:]):
                temp_elements = []

                for element in all_elements:
                    if not class_:
                        new_elements = list(element.find_all(tag, recursive=False))
                        new_elements = filter(lambda x: not x.has_attr('class') or not filter_illegal_classes(x.get('class')), new_elements)
                    else:
                        new_elements = list(element.find_all(tag, class_=class_, recursive=False))
                        new_elements = filter(lambda x: set(class_) == set(filter_illegal_classes(x.get('class'))), new_elements)

                    temp_elements.extend(new_elements)

                all_elements = temp_elements

            titles = [element.get_text().strip() for element in all_elements]

            return titles, 'ok'

        except:
            return traceback.format_exc(), 'exception'

    def _handle_http(self, source):
        scrape_start = time.time()

        articles = list(Article.objects.filter(source=source).order_by('-_created'))
        if not articles:
            result, status = None, 'no article'

        else:
            for article in articles:
                text = article.title

                result, status = self._handle_http_base(source.url, text)

                if status == 'ok':
                    break

        if status == 'ok':
            new_titles = self._have_titles(source, result)

            source.last_error = None
            source.error_count = 0
            source.enabled = True

            source.scrape_count += 1
            source.article_count += len(new_titles)

            source.scrape_time = time.time() - scrape_start
            source._last_success = self.now

        else:
            source.last_error = result
            source.error_count += 1

        if source.error_count > 20:
            source.enabled = False

        source._scraped = self.now
        source._next_scrape = self.now + timedelta(seconds=60)
        source.status = status
        source.save()

        return status

    def handle_add(self, **kwargs):
        text = kwargs['text']
        type = kwargs['type']

        if type == 'http':
            url = kwargs['url']
            # print("TRY HTTP")
            result, status = self._handle_http_base(url, text)
        elif type == 'curl':
            # print("TRY CURL")
            url = kwargs['url']
            curl = open(kwargs['source']).read()
            result, status = self._handle_curl_base(curl, text)

        if status == 'ok':
            for title in result:
                print(title)
            print()
            print("OK!!")

            if not kwargs['dry_run']:
                if ArticleSource.objects.filter(symbol__iexact=kwargs["symbol"]).count():
                    raise Exception("Already have a source with symbol \"{}\"".format(kwargs["symbol"]))

                source = ArticleSource.objects.create(
                    symbol=kwargs['symbol'].upper(),
                    url=kwargs['url'],
                    type=type,
                    enabled=True,
                )

                if type == 'curl':
                    source.data = curl
                    source.save()

                Article.objects.create(
                    source=source,
                    title=text
                )

                print("SUCCESSFULLY ADDED SOURCE {}".format(kwargs['symbol']))

        else:
            print("result", result, status)
            print("EXCEPTION")
