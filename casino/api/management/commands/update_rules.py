from django.core.management.base import BaseCommand
from tools.rules import generate_rules

import json
import re
import requests
import time

all_rules = {
    # "ceo":  # is max
    # {"value": "((ceo OR cfo OR cio) OR (chief officer) OR (board directors)) (((welcomes OR welcome OR step OR stepped OR stepping OR steps) (down OR up)) OR (remove OR removes OR removed OR removing) OR (present OR presents OR presenting OR presented) OR (replace OR replaces OR replacing OR relpaced)) -is:quote  -is:retweet -is:reply lang:en", "tag": "leadership"},
    # #
    # # "MEME":
    # # {"value": "((to the moon) OR (apes strong)) -is:quote  -is:retweet lang:en", "tag": "meme"},
    # #
    # # "PLUMMET":
    # # {"value": "(plummet OR plummets OR plummeting OR collapse OR collapses OR collapsing OR crash OR crashes OR crashing OR nosedive OR (nose dive) OR (dip OR dips OR dipping)) (stock OR shares OR premarket OR market) -is:retweet lang:en", "tag": "plummet"},
    # #
    # # "SOAR":
    # # {"value": "(rise OR rises OR rising OR ((shoot shooting shoots) up) OR spike OR spikes OR spiking OR soar OR soars OR soaring OR skyrocket OR skyrockets OR skyrocketing OR (bull run) OR bullish OR bulls) (stock OR shares OR premarket OR market) -is:retweet lang:en", "tag": "soar"},
    #
    # # "reddit":
    # # {"value": '(#wsb OR wsb OR wallstreetbets OR "r/stocks" OR "r/wallstreetbets" OR "r/investing" OR "r/stockmarket" OR "r/options" OR r/shortsqueeze/) -is:retweet lang:en', "tag": "reddit"},
    #
    # "gov":
    # {"value": '-state ("biden signs" ("justice department" investigating) OR "senate passes" OR "senate blocks" OR "biden to sign" OR "court blocks" OR "court upholds" OR (biden "executive order")) -is:quote -is:reply  -is:retweet lang:en', "tag": "gov"},
    #
    # "pharma":
    # {"value": "-petition (((medical OR clinical) (trial OR trials)) OR ((FDA OR EMA) (clearance OR decision OR decide OR decides OR approves OR approved OR approval OR approved OR approval OR review OR finds OR finding OR says)) OR ((breakthrough OR new) (drug OR medicine OR vaccine OR treatment))) -is:quote -is:reply  -is:retweet lang:en", "tag": "pharma"},
    #
    # # "PATENT":
    # # {"value": "((patent OR patents) (approved OR granted)) -is:quote  -is:retweet lang:en", "tag": "patent"},
    #
    # "scandal":
    # {"value": '("securities violations" OR ("breach of" duty) OR (claims against) OR ((launches OR launched OR launching) investigation)) -is:quote -is:reply  -is:retweet lang:en', "tag": "investigation"},
    #
    # "verdict":
    # {"value": '(((wins OR "hands down" OR overturns) (verdict OR case) (versus OR against)) OR "patent infringement") -is:quote -is:reply  -is:retweet lang:en', "tag": "verdict"},
    #
    # "news":
    # {"value": '("breaking news" OR "big scoop" OR "great scoop" OR "big news" OR "news alert" OR bloomberg OR "big announcement") -is:quote -is:reply  -is:retweet lang:en', "tag": "finance"},
    #
    # "oil":
    # {"value": '-massage -yemen (OPEC OR (oil (saudi OR saudis OR usa OR "united states" OR russia OR venezuela OR china))) -is:quote -is:reply  -is:retweet lang:en', "tag": "oil"},
    #
    # "merge":
    # {"value": "(merge OR merges OR merger OR merged OR acquire OR acquired OR acquisition OR acquires) -is:quote -is:reply  -is:retweet lang:en -club -clubs", "tag": "merge"},
    #
    # "contract":
    # {"value": "(contract OR bid OR pact OR treaty) (sign OR signed OR signs OR signing OR award OR awards OR awarding OR awarded OR grant OR grants OR granted OR granting OR win OR wins OR winning OR won OR receive OR receiving OR receives OR received) -is:quote -is:reply  -is:retweet lang:en", "tag": "contract"},
    #
    # "uncontract":
    # {"value": "(contract OR deal OR bid OR pact OR treaty) (block OR blocked OR blocks OR blocking OR lose OR lost OR loses OR losing OR break OR breaks OR breaking OR broke OR breach OR breaching OR breaches OR breached) -is:quote -is:reply  -is:retweet lang:en", "tag": "uncontract"},
    #
    # "geo": # explosions/sanctions/border disputes/assasination/
    # {"value": '("boeing crashes" OR "oil spil" OR "chemical disaster" OR "massive explosion") -is:quote -is:reply  -is:retweet lang:en', "tag": "geo"},

    "__vip_1":
        {
            "value": "(from:SquawkCNBC OR from:UOAMarkets OR from:InaraBright OR from:Luckinvest17 OR from:StckPro OR from:stock_titan OR from:SoFlTrader OR from:carlquintanilla OR from:LadeBackk OR from:thestreamable OR from:cnbc OR from:aljazeera OR from:Newsquawk OR from:LlcBillionaire OR from:deitaone OR from:nourhammoury OR from:breakingmkts OR from:US_FDA OR from:CNBCTheExchange OR from:markets OR from:bbgindustry OR from:bgov OR from:middleeast OR from:elonmusk OR from:Trade_The_News OR from:PeteyBullish OR from:psk2329a)",
            "tag": "__vip_1"
        },
    "__vip_2":
        {
            "value": "(from:sellvolbuytesla OR from:variety OR from:SeekingAlpha OR from:ema_news OR from:Blsellhigh OR from:twitapp7)",
            "tag": "__vip_2"
        },

    # "misc": {
    #     "value": '((evergreen OR "ever given") suez) lang:en -is:reply -is:retweet -is:quote',
    #     "tag": "misc",
    # }
}

subsets = {
    "oil": ["oil", "geo"],
    "all": list(all_rules.keys()),
}

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--rules', type=str, default='')
        parser.add_argument('--action', type=str, default='show')
        parser.add_argument('--subset', type=str, default='all')

    def _get_rules(self):
        url = 'https://api.twitter.com/2/tweets/search/stream/rules'
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer AAAAAAAAAAAAAAAAAAAAADOa8QAAAAAAJFmp2%2BRt94B7LwkA3hCEhrJNrik%3Dlg6D0G9k2kDEM2lARDG2sbbYUTfxy3eyKwIZUecHeuSg5RxZrI"
        }

        response = requests.get(url=url, headers=headers)
        if response.status_code != 200:
            print(response.content)
            raise("HTTP {}".format(response.status_code))

        data = json.loads(response.content)

        lookup = {}
        for rule in data.get('data', []):
            lookup[rule['id']] = rule
            lookup[rule['tag'].lower()] = rule

        return lookup

    def _delete_rules(self, ids):
        url = 'https://api.twitter.com/2/tweets/search/stream/rules'
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer AAAAAAAAAAAAAAAAAAAAADOa8QAAAAAAJFmp2%2BRt94B7LwkA3hCEhrJNrik%3Dlg6D0G9k2kDEM2lARDG2sbbYUTfxy3eyKwIZUecHeuSg5RxZrI"
        }
        data = {"delete": {"ids": ids}}

        response = requests.post(url, headers=headers, json=data)
        print(response.content)
        if response.status_code != 200:
            raise Exception("HTTP {}".format(response.status_code))

    def _add_rules(self, rules):
        url = 'https://api.twitter.com/2/tweets/search/stream/rules'
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer AAAAAAAAAAAAAAAAAAAAADOa8QAAAAAAJFmp2%2BRt94B7LwkA3hCEhrJNrik%3Dlg6D0G9k2kDEM2lARDG2sbbYUTfxy3eyKwIZUecHeuSg5RxZrI"
        }
        data = { "add": rules }

        response = requests.post(url, headers=headers, json=data)
        print(response.content)
        if response.status_code != 201:
            raise Exception("HTTP {}".format(response.status_code))

    def handle(self, *args, **kwargs):
        rule_lookup = self._get_rules()

        print("RULE LENGTH")
        for key in rule_lookup.keys():
            if re.match(r'\d+', key):
                continue
            rule = rule_lookup[key]
            print(rule["tag"], len(rule["value"]))

        if kwargs['action'] == 'show':
            for tag in sorted(rule_lookup.keys()):
                if re.match(r'\d+', tag):
                    continue
                print()
                print("tag ", tag)
                print("value", rule_lookup[tag])

            if not rule_lookup:
                print("(no rules)")

        elif kwargs['action'] == 'update':
            if kwargs['rules']:
                add_rule_tags = kwargs['rules'].split(',')
                delete_rule_ids = list(filter(None, [rule_lookup[tag]['id'] for tag in add_rule_tags if tag in rule_lookup]))
            else:
                delete_rule_ids = [key for key in rule_lookup.keys() if re.match(r'\d+', key) and not rule_lookup[key]['tag'].startswith('__symbols')]
                add_rule_tags = subsets[kwargs['subset']]

            print("deleting tags: ", [rule_lookup[rule_id]['tag'] for rule_id in delete_rule_ids])
            if delete_rule_ids:
                self._delete_rules(delete_rule_ids)

            time.sleep(5)
            print("adding tags: ", add_rule_tags)
            rules = [all_rules[tag] for tag in add_rule_tags]
            if rules:
                self._add_rules(rules)

        elif kwargs['action'] == 'delete':
            if kwargs['rules']:
                rule_tags = kwargs['rules'].split(',')
                rule_ids = [rule_lookup[tag]['id'] for tag in rule_tags if tag in rule_lookup]
            else:
                rule_ids = [key for key in rule_lookup.keys() if re.match(r'\d+', key)]

            print("deleting tags: ", [rule_lookup[rule_id]['tag'] for rule_id in rule_ids])
            if rule_ids:
                self._delete_rules(rule_ids)




