# Generated by Django 2.1 on 2021-02-21 11:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20210221_1123'),
    ]

    operations = [
        migrations.CreateModel(
            name='TwitterAuthor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('screen_name', models.CharField(max_length=64)),
                ('priority', models.IntegerField()),
            ],
        ),
    ]
