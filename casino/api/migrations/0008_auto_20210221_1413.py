# Generated by Django 2.1 on 2021-02-21 14:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_stockprice'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stockprice',
            name='last_price',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
