from django.db import models

# Create your models here.
class Stock(models.Model):
    symbol = models.CharField(max_length=12)
    last_seen = models.DateTimeField(null=True, blank=True, auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}'.format(self.symbol)

class StockPrice(models.Model):
    symbol = models.CharField(max_length=12)
    last_price = models.FloatField(null=True, blank=True)
    last_update = models.DateTimeField(null=True, blank=True, auto_now=True)

class TwitterAuthor(models.Model):
    screen_name = models.CharField(max_length=64, unique=True)
    priority = models.IntegerField()

    def __str__(self):
        return self.screen_name

class ArticleSource(models.Model):
    symbol = models.CharField(max_length=12)
    url = models.URLField()
    type = models.CharField(max_length=12)  # http, json
    data = models.CharField(max_length=4096, null=True, blank=True, default=None) # body, headers
    status = models.CharField(max_length=24, null=True, blank=True, default='') # no article/ok/broken
    last_error = models.CharField(max_length=10000, null=True, blank=True, default=None)
    scrape_count = models.IntegerField(blank=True, default=0)
    article_count = models.IntegerField(blank=True, default=0)
    scrape_time = models.FloatField(blank=True, default=None, null=True)
    enabled = models.BooleanField(blank=True, default=True)
    error_count = models.IntegerField(blank=True, default=0)

    _created = models.DateTimeField(auto_now_add=True)
    _updated = models.DateTimeField(auto_now=True)
    _next_scrape = models.DateTimeField(null=True, blank=True)
    _last_success = models.DateTimeField(null=True, blank=True)
    _last_new_article = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '{} - {}'.format(self.symbol, self.type)

class Article(models.Model):
    source = models.ForeignKey(ArticleSource, default=None, null=True, blank=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=1024)

    _created = models.DateTimeField(auto_now_add=True)

class Config(models.Model):
    tag = models.CharField(max_length=32)
    json = models.CharField(max_length=8192)