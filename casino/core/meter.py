from api.models import Stock
from datetime import datetime, timezone, timedelta

import time


class Meter:
    def __init__(self):
        self.config_data = {}

        self.dummy_key = 1
        self.posts = {}
        self.rate = {}
        self.zero_symbols = set()

    def update_config_data(self, config_data):
        for data_hash in set(self.config_data.keys()) - set(config_data.keys()):
            del self.posts[data_hash]

        for data_hash in set(config_data.keys()) - set(self.config_data.keys()):
            self.posts[data_hash] = []

        self.config_data = config_data

    def compile_data(self):
        data = {}
        for config_hash, config in self.config_data.items():
            chart_data = []
            if config['type'] == 'feed':
                chart_data = self.get_feed_data(config_hash, config)
            elif config['type'] == 'rate':
                chart_data = self.get_rate_data(config_hash, config)

            data[config_hash] = {'chart_data': chart_data}

        return data

    def get_feed_data(self, config_hash, config):
        size = int(config.get('size', 1))

        if config_hash not in self.posts:
            return []

        cutoff = (datetime.now(timezone.utc) - timedelta(seconds=size)).strftime("%Y-%m-%dT%H:%M:%S")

        try:
            posts = [{'type': 'dummy', 'count': 0}]
            for i, post in enumerate(self.posts[config_hash][::-1]):
                if post['type'] == 'dummy':
                    if i == 0:
                        posts[0] = post
                elif size == 0 or post['received_at'] > cutoff:
                    posts.append(post)
                else:
                    break

        except:
            print(post)

            raise

        return posts

    def get_rate_data(self, config_hash, config):
        # data = [
        #     {
        #         "order": 0,
        #         "symbol": 'aapl',
        #         "points": [
        #             {"timestamp": 0, "value": 0.1},
        #             {"timestamp": 1, "value": 0.3},
        #             {"timestamp": 2, "value": 0.2},
        #            ],
        #        },
        #     {
        #         "order": 1,
        #         "symbol": 'tlry',
        #         "points": [
        #             {"timestamp": 0, "value": 0.2},
        #             {"timestamp": 1, "value": 0.4},
        #             {"timestamp": 2, "value": 0.1},
        #             {"timestamp": 3, "value": 0.2},
        #            ],
        #        },
        #    ]

        # print("CONFIG", config)

        size = int(config['size'])

        second = int(time.time())

        for symbol in self.rate:
            value = self.update_value(symbol)
            if value < 0.01:
                self.zero_symbols.add(symbol)

            c = self.rate[symbol]['current']
            h = self.rate[symbol]['history']

            if h and h[-1]['timestamp'] == second:
                continue

            h.append({
                'timestamp': second,
                'value': c[0]
            })

        data = []
        for symbol in self.rate:
            if symbol in self.zero_symbols:
                continue
            data.append({
                "symbol": symbol,
                "points": self.rate[symbol]['history'][-size:]
            })

        data = sorted(data, key=lambda x: -x['points'][-1]['value'])

        for order, item in enumerate(data):
            item['order'] = order

        return data

    def mark(self, post, is_filtered):
        for config_hash in self.config_data:
            if is_filtered:
                if not self.posts[config_hash] or self.posts[config_hash][-1]['type'] != 'dummy':
                    self.posts[config_hash].append({'type': 'dummy', 'count': 1, 'key': self.dummy_key})
                    self.dummy_key += 1
                else:
                    self.posts[config_hash][-1]['count'] += 1
            else:
                self.posts[config_hash].append(post)

        if is_filtered:
            return

        now = datetime.now(timezone.utc)
        for symbol in post['symbols']:
            if symbol not in self.rate:
                self.rate[symbol] = {
                    'current': [0, time.time()],
                    'history': []
                }

            self.rate[symbol]['current'][0] = self.update_value(symbol) + 1.0

            try:
                stock = Stock.objects.get(symbol__iexact=symbol.lower())
                stock.last_seen = now
                stock.save()
            except Exception as e:
                Stock.objects.create(symbol=symbol, last_seen=now)

            if symbol in self.zero_symbols:
                self.zero_symbols.remove(symbol)

    def update_value(self, symbol):
        s = self.rate[symbol]['current']

        if s[0] == 0:
            s[1] = time.time()

            return 0
        else:
            value = s[0] * (0.95 ** (time.time() - s[1]))
            s[0] = value
            s[1] = time.time()

            return value

