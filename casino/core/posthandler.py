from api.models import Stock
from api.consumers import Consumer
from core.meter import Meter
from datetime import datetime, timedelta, timezone
from tools.queues import enqueue, dequeue

import elasticsearch.helpers
import html
import json
import math
import re
import requests
import sys
import time
import threading


class PostHandler:
    INSTANCE = None

    @staticmethod
    def instance():
        if PostHandler.INSTANCE is None:
            PostHandler.INSTANCE = PostHandler()

        return PostHandler.INSTANCE

    @staticmethod
    def process(source, posts):
        for post in posts:
            PostHandler.instance().process_post(source, post)

    def __init__(self):
        self.meter = Meter()
        self.capstag_regex = None
        self.cashtag_regex = None
        self.company_regex = None

        self.stock_data = None
        self.post_lookup = None
        self.symbol_lookup = None

        self.twitter_rate = {
            'start': time.time(),
            'count': 0,
            'rules': {},
            'current_count': 0,
            'current_start': time.time()
        }

        self.es = elasticsearch.Elasticsearch(hosts=['http://localhost:9200'])

        self.init_stock_data()
        self.init_regexes()
        self.init_lookups()

        self.stop_processing = False
        self.threads = self.init_threads()

        self.scores = []

        self.black_list = []
        self.black_list_load_timestamp = 0

    def __del__(self):
        self.stop_processing = True
        for thread in self.threads:
            thread.join()

    def init_threads(self):
        config_thread = threading.Thread(target=self.fetch_configurations)
        config_thread.start()

        time.sleep(0.3)

        report_thread = threading.Thread(target=self.report_data)
        report_thread.start()

        return [config_thread, report_thread]

    def init_regexes(self):
        self.cashtag_regex = re.compile(r'(?<=\$)(' + r'|'.join(self.stock_data.keys()) + r')(?![a-zA-Z0-9])', re.IGNORECASE)

        long_symbols = [symbol for symbol in map(str.upper, self.stock_data.keys()) if len(symbol) > 2]
        long_symbols = sorted(set(long_symbols) - {'KING'})
        self.capstag_regex = re.compile(r'(?<![a-zA-Z0-9])(' + r'|'.join(long_symbols) + r')(?![a-zA-Z0-9])')

        company_names = sorted(company['clean'] for company in self.stock_data.values())
        for i, name in enumerate(company_names):
            parts = re.split(r'(\W+)', name)
            for j, part in enumerate(parts):
                if re.match(r'\w+', part):
                    parts[j] = part.capitalize()
            company_names[i] = ''.join(parts)

        for company in self.stock_data.values():
            company_names.extend(company['alternatives'])
        company_names = map(html.escape, company_names)

        self.company_regex = re.compile(r'(?<![a-zA-Z0-9])(' + r'|'.join(company_names) + r')(?![a-zA-Z0-9])')

    def init_lookups(self):
        self.post_lookup = dict()

        parameters = {
            'client': self.es,
            'index': 'posts',
            'scroll': '5m',
            'query': {"query": {"bool": {"must": [
                { "bool": { "should": [
                    {"range": {"received_at": {"gte": "now-1w"}}},
                    {"range": {"indexed_at": {"gte": "now-1w"}}},
                ]}},
                {"match": {"source": "twitter"}}
            ]}}}
        }

        try:
            count = 0
            for doc in elasticsearch.helpers.scan(**parameters):
                count += 1
                clean_text = self.get_clean_text(doc['_source']['text'])

                self.post_lookup[clean_text] = doc['_source']

            self.symbol_lookup = {self.stock_data[key]['clean'].lower(): key for key in self.stock_data.keys()}
            for key, value in self.stock_data.items():
                for alternative in value['alternatives']:
                    self.symbol_lookup[alternative.lower()] = key
            print("preloaded {} posts".format(count))
        except Exception as e:
            raise Exception("Is ES running?")

    def init_stock_data(self):
        file_name = '/home/peter/programming/stock/data/stock_data.txt'

        data = eval(open(file_name).read())
        data = {item[0]: {'name': item[1], 'clean': item[2], 'alternatives': item[3:]} for item in data}

        self.stock_data = data

    def process_post(self, source, post):
        if source == 'twitter':
            self.track_twitter_rate(post)

        self.enrich_post(post)

        filter_reason = self.filter_post(post)
        if not filter_reason:
            print("post", post['text'][:40])
        else:
            print(filter_reason, post['text'][:40])

        post['filter_reason'] = filter_reason
        self.meter.mark(post, True if filter_reason else False)
        self.index_post(post)

    def index_post(self, post):
        enqueue(post, 'index')

    def get_clean_text(self, text):
        clean_text = text

        clean_text = re.sub(r'\s+https?://t\.co/\w+', '', clean_text)
        # remove mentions and hashtags
        clean_text = re.sub(r'[#@]\w+', '', clean_text)
        # remove non alphanumerics
        clean_text = re.sub(r'\W', '', clean_text).lower()[:60]

        return clean_text

    def filter_post(self, post):
        if not post or 'text' not in post:
            return 'NO OR EMPTY POST'

        now = time.time()
        if now > self.black_list_load_timestamp + 60:
            file_name = '/home/peter/programming/stock/data/black_list.py'
            self.black_list_load_timestamp = now

            try:
                self.black_list = eval(open(file_name).read())
                print("blacklist", self.black_list)
                self.black_list = map(lambda x: re.compile(x, re.IGNORECASE), self.black_list)
            except:
                print("COULD NOT UPDATE BLACKLIST")

        if 'microsoft' in post['text'].lower() and 'discord' in post['text'].lower():
            pass

        for regex in self.black_list:
            if regex.search(post['text']):
                return 'BLACKLIST'

        if len(re.findall(r'#\w+', post['text'])) > 5:
            return 'HASHTAG SPAM'

        if len(re.findall(r'\d+[,\.]\d+%', post['text'])) > 5:
            return 'PERCENTAGE SPAM'

        if len(re.findall(r'@\w+', post['text'])) > 5:
            return 'MENTION SPAM'

        if len(re.findall(r'\$[a-zA-Z]{2,}', post['text'])) > 5:
            return 'CASHTAG SPAM'

        banned_phrases = [
            'free stock',
            'professional chat'
        ]

        for phrase in banned_phrases:
            if phrase.lower() in post['text']:
                return 'BANNED PHRASE'

        authors = set(map(str.lower, ['jimytwits', 'wsbchairman_', 'tickeron', 'AfafAlakwa', 'Traderofstocks1']))
        if post['author'].lower() in authors:
            return 'SPAM AUTHOR'

        if post['source'] != 'press':
            clean_text = self.get_clean_text(post['text'])
            print("CLEAN TEXT", clean_text)
            if clean_text in self.post_lookup:
                # print("NOT ORIGINAL")
                original = self.post_lookup[clean_text]
                if (time.time() - original.get('seen_at', 0)) > 20:
                    return 'OLD DUPLICATE'
            else:
                post['seen_at'] = time.time()
                self.post_lookup[clean_text] = post
                # print("ORIGINAL")
                # print(post)

        # always allow gov and oil posts
        if set(post['tags']) & {'gov', 'oil'}:
            return None

        # always show if post contains stock we follow
        if 'stock' in set(post['symbols']):
            return None

        if len(post['symbols']) == 0:
            return 'NO SYMBOLS'

        return None

    def enrich_post(self, post):
        if post['source'] == 'twitter':
            self.enrich_twitter_post(post)
        elif post['source'] == 'reddit':
            self.enrich_reddit_post(post)

        # do ranking
        post['ranking'] = {}

        # START TEXT RANKING
        # query = {
        #     "query": {
        #         "bool": {
        #             "must": [
        #                 {
        #                     "match": {
        #                         "text": post['text']
        #                     }
        #                 },
        #                 {
        #                     "range": {
        #                         "received_at": {
        #                             "gte": "now-1d"
        #                         }
        #                     }
        #                 }
        #             ]
        #         }
        #     },
        #     "sort": {
        #         "_score": "desc"
        #     },
        #     "size": 1
        # }
        #
        # response = self.es.search(body=query, index='posts')
        #
        # thresholds = [0, 14.4, 17.6, 20.8, 24.5, 28.9, 38.6, 9999.0]
        # for hit in response['hits']['hits']:
        #     score = hit['_score']
        #
        #     for i, t in enumerate(thresholds):
        #         if score > t:
        #             post['ranking']['text'] = 6 - i
        #         else:
        #             break
        #
        #     break

        # START AGE RANKING
        # max_age = 1
        # now = datetime.now(timezone.utc)
        # for symbol in post['symbols']:
        #     try:
        #         stock = Stock.objects.get(symbol=symbol)
        #         max_age = max(max_age, (now - stock.last_seen).total_seconds())
        #         stock.last_seen = now
        #         stock.save()
        #     except:
        #         stock = Stock.objects.create(symbol=symbol, last_seen=now)
        #         max_age = 3600
        #
        # post['ranking']['age'] = int(max(0, min(6, math.log(max_age, 4))))

    def enrich_reddit_post(self, post):
        post['text'] = post['full_text'][:300] if len(post['full_text']) < 300 else post['full_text'][:297] + '...'
        post['html'] = post['text']

        all_text = post['title'] + ' ' + post['full_text']
        post['symbols'] = sorted(set(self.cashtag_regex.findall(all_text)) | set(self.capstag_regex.findall(all_text)))

        if 'oil' in post['tags'] and 'oil' not in post['symbols']:
            post['symbols'].append('oil')

        post['symbol_opacity'] = {}
        now = datetime.now(timezone.utc)
        for symbol in post['symbols']:
            try:
                stock = Stock.objects.get(symbol=symbol)
                recency = (now - stock.last_seen).total_seconds()
                opacity = max(1, min(10, math.log(recency, 4)))
                post['symbol_opacity'][symbol] = opacity / 10.0
            except:
                post['symbol_opacity'][symbol] = 1

    def enrich_twitter_post(self, post):
        post['symbols'] = []

        markup = []
        for match in self.cashtag_regex.finditer(post['text']):
            symbol = post['text'][match.start():match.end()].lower()

            markup.append([match.start(), match.end(), "symbol", symbol, 1])

            post['symbols'].append(symbol)

        # for match in self.capstag_regex.finditer(post['text']):
        #     symbol = post['text'][match.start():match.end()].lower()
        #
        #     markup.append([match.start(), match.end(), "symbol", symbol[1:], 4])
        #
        #     post['symbols'].append(symbol)

        for match in self.company_regex.finditer(post['text']):
            name = match.string[match.start():match.end()].lower()
            symbol = self.symbol_lookup[html.unescape(name)]

            if symbol in post['symbols']:
                continue

            markup.append([match.start(), match.end(), "symbol", symbol, 2])

            post['symbols'].append(symbol)

        if 'oil' in post['tags']:
            post['symbols'].append('oil')

        post['symbols'] = list(sorted(set(post['symbols'])))

        post['symbol_opacity'] = {}
        now = datetime.now(timezone.utc)
        for symbol in post['symbols']:
            try:
                stock = Stock.objects.get(symbol=symbol)
                recency = (now - stock.last_seen).total_seconds()
                opacity = max(1, min(10, math.log(recency, 4)))
                post['symbol_opacity'][symbol] = opacity / 10.0
                stock.last_seen = now
                stock.save()
            except:
                Stock.objects.create(symbol=symbol, last_seen=now)
                post['symbol_opacity'][symbol] = 1


        all_urls = re.findall(r'(?<!\w)https?://t\.co/\w+', post['text'])
        for url in all_urls:
            start = post['text'].find(url)
            markup.append([start, start+len(url), "link", None, 3])

        markup = sorted(markup, key=lambda x: x[0])
        for i in range(len(markup) - 1):
            if markup[i] is None:
                continue

            a = markup[i]
            b = markup[i+1]

            if a[1] > b[0]:  # interference
                if a[4] <= b[4]:  # a has higher priority(lower number), so delete b
                    markup[i+1] = None
                else:
                    markup[i] = None

        markup = list(filter(None, markup[::-1]))

        html_body = post['text']
        for item in markup:
            start, end, action, data, priority = item

            prefix = html_body[0: start]
            link = html_body[start: end]
            linkText = '&#8599;'
            suffix = html_body[end:]

            if action == 'symbol':
                linkStart = '<a href="https://www.etoro.com/markets/{}/chart/full" target="_blank">'.format(data)
                html_body = prefix + linkStart + link + '</a>' + suffix
            elif action == 'link':
                linkStart = '<a href="{}" target="_blank">'.format(link)
                html_body = prefix + linkStart + linkText + '</a>' + suffix

        post['html'] = html_body

    def report_data(self):
        while not self.stop_processing:
            start_time = time.time()

            sys.stdout.write('r')
            sys.stdout.flush()

            twitter_rate = self.twitter_rate['count'] / (time.time() - self.twitter_rate['start'])

            data = self.meter.compile_data()
            for config_hash in data:
                data[config_hash]['status'] = {'twitter_rate': twitter_rate}
                data[config_hash]['message'] = 'chart_data'

            # enqueue(data, 'notify')
            requests.post("http://localhost:8000/site/notify", json=data)

            sleep = 1.0 - time.time() + start_time
            if sleep > 0:
                time.sleep(sleep)

        print("DONE REPORTING")

    def fetch_configurations(self):
        counter = 0
        while not self.stop_processing:
            start_time = time.time()

            # sys.stdout.write('c')

            try:
                response = requests.get("http://localhost:8000/site/notify")

                config_data = json.loads(response.content)

                self.meter.update_config_data(config_data)
            except:
                print("Problem getting config data")

            sleep = 1.0 - time.time() + start_time
            if sleep > 0:
                time.sleep(sleep)

            counter += 1

    def track_twitter_rate(self, post):
        self.twitter_rate['count'] += 1
        self.twitter_rate['current_count'] += 1

        for tag in post['tags']:
            if tag not in self.twitter_rate['rules']:
                self.twitter_rate['rules'][tag] = 0
            self.twitter_rate['rules'][tag] += 1

        if self.twitter_rate['count'] % 10 == 0:
            rate = self.twitter_rate['count'] / (time.time() - self.twitter_rate['start'])
            print()
            print('STATUS RATE: {} tweets per second'.format(rate))
            rule_shares = sorted((value, key) for key, value in self.twitter_rate['rules'].items())[::-1]
            for count, tag in rule_shares:
                print('  RULE RATE for {}: {}, {} tweets per second'.format(tag, count, int(100.0 * count / self.twitter_rate['count']) / 100))

            self.twitter_rate['current_count'] /= 2.0
            self.twitter_rate['current_start'] = self.twitter_rate['current_start'] + (time.time() - self.twitter_rate['current_start']) / 2.0
