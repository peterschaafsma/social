import * as d3 from "d3";

import queryString from 'query-string';
import React from "react";
import ReactDOM from "react-dom";
import { Button, Form, Badge } from "react-bootstrap";
import { BrowserRouter, Redirect, Route } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPause, faPlay } from '@fortawesome/free-solid-svg-icons'
import { faGoogle, faTwitter } from '@fortawesome/free-brands-svg-icons'
import { Typeahead } from 'react-bootstrap-typeahead';
import runes from 'runes';

import './App.scss';

const hostname = location.hostname;

class PostDots extends React.Component {
    render() {
//        console.log('postdots', this.props);

        const postDots = [...Array(this.props.count).keys()].map(i => {
            return <div key={i} className="post-dot fade"/>
        });

        return (
            <div className="post-dots-container">
                <div className="post-dots">
                    <div key="-1" className="post-dot"/>
                    {postDots}
                </div>
            </div>
        );
    }
}

class Post extends React.Component {
    render() {
        const post = this.props.post;
        let authorPrefix;
        if (post.source == 'twitter') {
            authorPrefix = '@';
        }
        else if (post.source == 'reddit') {
            authorPrefix = 'u/';
        }
        else {
            authorPrefix = '';
        }

        let author = `${authorPrefix}${post.author}`;
        if (post.followers_count) {
            author = author + ` (${post.followers_count})`;
        }
        const authorUrl = post.author_url;
        const createdAt = this.formatDate(post.created_at);
        const postUrl = post.post_url;

//        console.log(post.symbol_opacity);

        const stockBadges = post.symbols.map(symbol => {
//            console.log("symbol", symbol);
            const className = `badge symbol`;
            const etoroUrl = `https://www.etoro.com/markets/${symbol}/chart/full`;
            const marketWatchUrl = `https://www.marketwatch.com/investing/stock/${symbol}`;
            const onClick = e => {
                let url;
                if (e.ctrlKey) {
                    url = marketWatchUrl;
                }
                else {
                    url = etoroUrl;
                }
                window.open(url, "_blank");
            }
            let opacity = 1;
            if (post.symbol_opacity) {
                opacity = post.symbol_opacity[symbol] || 1;
            }
            return <div key={symbol} style={{opacity: opacity}} className={className} onClick={onClick}>${symbol.toUpperCase()}</div>;
        });

        const rankBadges = Object.keys(post.ranking).map(rank => {
            const value = post.ranking[rank];

            const className = `badge rank level-${value}`;

            return <div key={rank} className={className}>{rank.toUpperCase()}</div>;
        });


        let tags;
        let title;
        let headline;
        let html;
        let body;

        let sourceName = post.source;
        if (post.tags.includes('vip')) {
            sourceName = 'vip';
        }
        else if (post.tags.includes('misc')) {
            sourceName = 'misc';
        }

        if (post.title) {
            tags = null; //post.tags.map(tag => `<b class="post-tag source-${sourceName}">r/${tag.toUpperCase()}</b>`).join('');

            html = `<p>${tags}&mdash;<span class="post-title">${post.title}</span></p>`;

            headline = (
                <div className="post-headline" dangerouslySetInnerHTML={{__html: html}}/>
            );

            body = (
                <div className="post-body" dangerouslySetInnerHTML={{__html: post.html}}/>
            );
        }
        else {
            tags = post.tags.map(tag => `<b class="post-tag source-${sourceName}">${tag.toUpperCase()}</b>`).join('');

            html = `<p>${tags}&mdash;<span class="post-body">${post.html}</span></p>`;

            headline = null;

            body = (
                <div className="post-body" dangerouslySetInnerHTML={{__html: html}}/>
            );
        }

//        console.log("post", post);
//        console.log("title", title);
        return (
            <div className="post flash" onClick={this.props.f.onClickPost} onMouseUp={this.props.f.onMouseUp}>
                <div className="post-header">
                    <div className="post-header-author">
                        <a href={authorUrl} target="blank">{author}</a>
                    </div>
                    <div className="post-header-createdate">
                        <a href={postUrl} target="blank">{createdAt}</a>
                    </div>
                </div>
                {headline}
                {body}
                <div className="post-footer">
                    <div className="stock-badges">
                        {stockBadges}
                    </div>
                    <div className="rank-badges">
                        {rankBadges}
                    </div>
                </div>
            </div>
        );
    }

    formatDate(date) {
        const createdAt = new Date(date);
        const options = {
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
            day: 'numeric',
            month: 'short',
        };

        return createdAt.toLocaleDateString("nl-NL", options);
    }
}

class Header extends React.Component {
    render() {
//        <Form.Control type="text" as="textarea" placeholder="Regex..." value={this.props.filterText} onChange={this.props.f.onChangeFilter}/>

        let applyButton = null;
//        applyButtion = (
//            <Button style={{marginLeft: "auto"}} variant="outline-primary" onClick={this.props.f.onClickApply}>
//                <span>Apply</span>
//            </Button>
//        );

        const icon = this.props.paused ? faPlay : faPause;
        return (
            <React.Fragment>
                <div>
                    <div className="flex" style={{marginTop: "2px"}}>
                        <Button variant="outline-primary" onClick={this.props.f.togglePaused}>
                            <FontAwesomeIcon icon={icon}/>
                        </Button>
                        <Button style={{marginLeft: "2px"}} variant="outline-primary" onClick={this.props.f.onGoogle}>
                            <FontAwesomeIcon icon={faGoogle}/>
                        </Button>
                        <Button style={{marginLeft: "2px"}} variant="outline-primary" onClick={this.props.f.onTwitter}>
                            <FontAwesomeIcon icon={faTwitter}/>
                        </Button>
                        {applyButton}
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

class Feed extends React.Component {
    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.paused) {
            return false;
        }

        return true;
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}

class FeedChart extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };

    }

    render() {
        if (! this.props.data) {
            return null;
        }

//        console.log("FeedChart props", this.props);

        const posts = this.props.data.map(post => {
            if (post.type == 'dummy') {
                return <PostDots key={post.key} count={post.count}/>;
            }
            else {
                return <Post key={post.id} post={post} f={this.props.f}/>;
            }
        });

        let stockTypeAhead = (
            <Typeahead
                id="0"
                selected={this.props.config.symbols}
                multiple={true}
                clearButton={true}
                placeholder="Select stocks..."
                options={this.props.context.symbols}
                onChange={this.props.f.onChangeSymbols}
            />
        );

        const rules = (
            <div>RULE</div>
        );

        let ruleSelector = null; (
            <div className="ruleselector">
                <div className="rules">
                    {rules}
                </div>
                <div className="rulecreator">
                    <div className="typeahead">
                        <Typeahead
                            id="1"
                            selected={this.props.config.rules}
                            multiple={true}
                            clearButton={true}
                            allowNew={true}
                            placeholder="Add rule..."
                            options={this.props.context.ruleElements}
                            onChange={this.props.f.onChangeRule}
                        />
                    </div>
                    <Button variant="outline-primary" onClick={this.props.f.addRule}>
                        <span>Add&nbsp;rule</span>
                    </Button>
                </div>
            </div>
        )

        ruleSelector = null;
        stockTypeAhead = null;

        return (
            <React.Fragment>
                {stockTypeAhead}
                {ruleSelector}
                <Header filterText={this.props.config.filter} f={this.props.f} paused={this.props.paused}/>
                <Feed paused={this.props.paused}>
                    {posts}
                </Feed>
            </React.Fragment>
        );
    }
}

class RateChart extends React.Component {
    constructor(props) {
        super(props);

        this.ref = React.createRef();
        this.svg = null;

        this.updatePlot = this.updatePlot.bind(this);
    }

    componentDidMount() {
        this.svg = d3.select(this.ref.current)
            .append('svg')
                .attr("viewBox", "0 0 1200 600")
                .attr("preserveAspectRatio", "xMidYMid");
        this.svg
            .append("g")
                .attr("class", "view");
    }

    componentDidUpdate(prevProps, prevState) {
        this.updatePlot();
    }

    updatePlot() {
        let data = this.props.data;

//        console.log("DATA", data);

//        return;

        // SCALE
        const minTimestamp = Math.min(...data.map(symbolData => symbolData.points.map(point => point.timestamp)[0]));
        const maxTimestamp = Math.max(...data.map(symbolData => symbolData.points.map(point => point.timestamp).slice(-1)[0]));
        const maxValue = Math.max(...data.map(symbolData => Math.max(...symbolData.points.map(point => point.value))));

        const scale = {};
        scale.x = d3.scaleLinear()
            .domain([minTimestamp, maxTimestamp])
            .range([0, 1000]);

        scale.y = d3.scaleLinear()
            .domain([maxValue+0.1,-0.1])
            .range([20, 580]);


        // LABEL HEIGHT
        const heightMap = {};
        let minHeight = 580;
        const cutoff = 0.01;
        data.slice().reverse().forEach(symbolData => {
            const value = symbolData.points.slice(-1)[0].value;
            if (value >= cutoff) {
                let height = scale.y(value);
                if (height > minHeight) {
                    height = minHeight;
                }
                minHeight = height - 20;
                heightMap[symbolData.symbol] = height;
            }
        });


        // LABELS
        const labels= this.svg.select("g.view").selectAll("text")
            .data(data);

        labels
            .enter()
            .append("text")
                .style("text-anchor", "start")
                .style("alignment-baseline", "central")
                .attr("class", symbolData => `plot-label order-${symbolData.order}`)
                .attr("font-size", "24px")
                .on('click', (event, symbolData) => window.open(`https://www.etoro.com/markets/${symbolData.symbol}/chart/full`, "_blank"));

        labels
            .exit()
            .remove();


        this.svg.select("g.view").selectAll("text")
            .text(symbolData => (symbolData.points.slice(-1)[0].value < cutoff ? "" : symbolData.symbol))
            .attr("transform", symbolData => `translate(1012, ${heightMap[symbolData.symbol] || 0})`);


        // LINE PLOT
        const plot = this.svg.select("g.view").selectAll("path")
            .data(data);

        plot
            .enter()
            .append("path")
                .attr("class", symbolData => `order-${symbolData.order}`);

        plot
            .exit()
            .remove();

        const curve = d3.line()
            .x(point => scale.x(point.timestamp))
            .y(point => scale.y(point.value))
            .curve(d3.curveMonotoneX);

        this.svg.select("g.view").selectAll("path")
            .attr("d", symbolData => curve(symbolData.points));
    }

    render() {
        return (
            <div ref={this.ref}/>
        );
    }
}


class Chart extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            config: {},
            data: [],
            status: {},
            paused: false,
            context: {
                symbols: [],
                ruleElements: [],
            },
            redirect: null
        };

        this.state.data = [
//            {"title":"Real question here from a fellow 🦍 about GMe and others","long_text":"My question fellow 🦍 is this. Why do I see many transactions in off hours, when the market closes. But this weekend not one single spike, and every site with a descent market watch reports different numbers. One says 99,50 others 101,74.\n\nHope to get a answer. Will hold forever, we need a lambo. My friends all drive porshes and we must make amends. 🚀🚀🚀🚀🚀🚀🚀 To Tha 🌑","long_text_html":"<!-- SC_OFF --><div class=\"md\"><p>My question fellow 🦍 is this. Why do I see many transactions in off hours, when the market closes. But this weekend not one single spike, and every site with a descent market watch reports different numbers. One says 99,50 others 101,74.</p>\n\n<p>Hope to get a answer. Will hold forever, we need a lambo. My friends all drive porshes and we must make amends. 🚀🚀🚀🚀🚀🚀🚀 To Tha 🌑</p>\n</div><!-- SC_ON -->","text":"My question fellow 🦍 is this. Why do I see many transactions in off hours, when the market closes. But this weekend not one single spike, and every site with a descent market watch reports different numbers. One says 99,50 others 101,74.\n\nHope to get a answer. Will hold forever, we need a lambo. My ...","author":"Knightfires","author_url":"https://reddit.com/user/Knightfires/","created_at":"2021-02-28T11:32:20.000000","post_url":"https://reddit.com/r/wallstreetbets/comments/ludtob/real_question_here_from_a_fellow_about_gme_and/","id":"ludtob","source":"reddit","tags":["wallstreetbets"],"symbols":["tlry", "aapl"],"html":"My question fellow 🦍 is this. Why do I see many transactions in off hours, when the market closes. But this weekend not one single spike, and every site with a descent market watch reports different numbers. One says 99,50 others 101,74.\n\nHope to get a answer. Will hold forever, we need a lambo. My ...","ranking":{}},
//            {"source":"twitter","text":"Join Robinhood with my link and we'll both get a free stock 🤝 $FCEL $TLRY $KODK $NIO 🚀🚀🚀 https://t.co/VZb6w9cA4M","author":"WSBChairman_","author_url":"https://twitter.com/WSBChairman_","followers_count":89,"created_at":"2021-02-28T19:54:28.000000+0000","id":"1366114565323444230","post_url":"https://twitter.com/WSBChairman_/status/1366114565323444230","tags":["reddit"],"symbols":[],"html":"Join Robinhood with my link and we'll both get a free stock 🤝 $FCEL $TLRY $KODK $NIO 🚀🚀🚀 <a href=\"https://t.co/VZb6w9cA4M\" target=\"_blank\">https://t.co/VZb6w9cA4M</a>","ranking":{}}
        ];

        this.selectedText = null;
        this.toggleTimeout = null;
        this.socket = null;

        this.updateState = this.updateState.bind(this);
        this.setupWebSocket = this.setupWebSocket.bind(this);
        this.onChangeFilter = this.onChangeFilter.bind(this);
        this.onClickApply = this.onClickApply.bind(this);
        this.unregisterChart = this.unregisterChart.bind(this);
        this.registerChart = this.registerChart.bind(this);
        this.onChangeSymbols = this.onChangeSymbols.bind(this);
        this.onClickPost = this.onClickPost.bind(this);
        this.onChangeRule = this.onChangeRule.bind(this);
        this.addRule = this.addRule.bind(this);
        this.onMouseUp = this.onMouseUp.bind(this);
        this.togglePaused = this.togglePaused.bind(this);
        this.onGoogle = this.onGoogle.bind(this);
        this.onTwitter = this.onTwitter.bind(this);

    }

    componentDidMount() {
        this.setupWebSocket();

        const values = queryString.parse(this.props.location.search);

        const config = {};
        const fields = ['type', 'size', 'category', 'filter', 'ranking', 'subtype', 'symbols'];
        fields.forEach(field => {
            if (field in values) {
                let value = values[field];
                if (field == 'symbols') {
                    value = value == '' ? [] : value.split(',');
                }
                config[field] = value;
            }
        });
        this.updateState({config: config});

        fetch(`http://${hostname}:8000/site/stock`)
        .then(response => response.json())
        .then(json => {
//            console.log("json", json);

            const context = {...this.state.context};
            context.symbols = json.symbols;
            context.ruleElements = json.rule_elements;

            this.updateState({context: context});
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.redirect) {
            this.unregisterChart();
            this.registerChart();

            this.updateState({redirect: null});
        }
    }

    render() {
        if (this.state.redirect) {
            return (
                <Redirect to={this.state.redirect}/>
            );
        }

        const rate = this.state.config.type == "rate";
        const feed = this.state.config.type == "feed";

        const props = {
            config: this.state.config,
            data: this.state.data,
            context: this.state.context,
            paused: this.state.paused,
            f: {
                onChangeFilter: this.onChangeFilter,
                onClickApply: this.onClickApply,
                onChangeSymbols: this.onChangeSymbols,
                onClickPost: this.onClickPost,
                onChangeRule: this.onChangeRule,
                onMouseUp: this.onMouseUp,
                togglePaused: this.togglePaused,
                onGoogle: this.onGoogle,
                onTwitter: this.onTwitter,
            },
        };

        const twitterRate = this.state.status.twitter_rate ? this.state.status.twitter_rate.toFixed(2) : 0;
        let lastUpdate = '-';
        if (this.state.status.last_update) {
            const l = this.state.status.last_update;

            lastUpdate = `${l.CET} ${l.EST}`;
        }
        const statusText = `Twitter rate: ${twitterRate} Last update: ${lastUpdate}`;

        return (
            <React.Fragment>
                <div className="status-bar">{statusText}</div>
                {rate && <RateChart {...props}/>}
                {feed && <FeedChart {...props}/>}
            </React.Fragment>
        );
    }

    updateState(update) {
        const state = {...this.state, ...update};

        this.setState(state);
    }

    setupWebSocket() {
        this.socket = new WebSocket(`ws://${hostname}:8000/`);

        this.socket.onmessage = message => {
            const data =  JSON.parse(message.data);

//            console.log("MESSAGE", data);

            if (data.message == 'signal') {
//                console.log(data.message)
            }
            else if (data.message == 'chart_data') {
//                console.log("status", data['status']);

                var now = new Date();
                data.status.last_update = {
                    CET: now.toLocaleTimeString('nl-NL', {timeZone: 'CET'}) + ' CET',
                    EST: now.toLocaleTimeString('en-US', {timeZone: 'EST'}) + ' EST',
                };

                this.updateState({status: data.status, data: data.chart_data});
            }
        }

        this.socket.onclose = e => {
            console.log("CLOSING");
        }

        this.socket.onopen = e => {
            console.log("OPEN");

            this.registerChart();
        }
    }

    onClickApply() {
        const search = queryString.parse(this.props.location.search);

        let changed = false;
        if (search.filter != this.state.config.filter) {
            search.filter = this.state.config.filter;
            changed = true;
        }
        if (search.symbols != this.state.config.symbols) {
            search.symbols = this.state.config.symbols.join(',');
            changed = true;
        }

        if (changed) {
            const redirect = {
                pathname: this.props.location.pathname,
                search: queryString.stringify(search),
                state: null
            };

            this.updateState({redirect: redirect});
        }
    }

    onChangeFilter(event) {
        const config = {...this.state.config};
        config.filter = event.target.value;
        this.updateState({config: config});
    }

    onChangeSymbols(selected) {
//        console.log("selected", selected);
//
//        return;

        const config = {...this.state.config, symbols: selected};

        this.updateState({config: config});
    }

    onChangeRule(selected) {
        const config = {...this.state.config, rule: selected};

        this.updateState({config: config});
    }

    addRule(event) {
        let rules = this.state.config.rules || [];

        rules.push([...this.state.config.rule]);

        const config = {...this.state.config, rules: rules};

        this.updateState({config: config});

    }

    onClickPost(event) {
        console.log("click", event);
        console.log(event.target.id);
    }

    escapeHTML(unsafeText) {
        let div = document.createElement('div');
        div.innerText = unsafeText;
        return div.innerHTML;
    }

    onGoogle(event) {
        if (this.selectedText) {
            const query = this.escapeHTML(this.selectedText);

            const url = `http://www.google.com/search?q=${query}`;

            window.open(url, "_blank");
        }
    }

    onTwitter(event) {
        if (this.selectedText) {
            const query = this.escapeHTML(this.selectedText);

            const url = `https://twitter.com/search?q=${query}&src=typed_query&f=live`;

            window.open(url, "_blank");
        }
    }

    onMouseUp(event) {
        const selection = window.getSelection();

        if (selection && selection.toString().length > 0) {
            console.log("has selection ", selection.toString());
            this.selectedText = selection.toString();

            this.togglePaused(true);
        }
        else {
            this.selectedText = "";
        }
    }

    togglePaused(paused) {
        if (paused == this.state.paused) {
            return;
        }

        const state = {...this.state};
        state.paused = !state.paused;
        this.setState(state);

        if (this.toggleTimeout) {
            console.log("clear timeout");
            window.clearTimeout(this.toggleTimeout);

            console.log("tt null");
            this.toggleTimeout = null;
        }

        const continueFeed = () => {
            console.log("tt null");
            this.toggleTimeout = null;

            console.log("continue feed");
            const state = {...this.state};
            state.paused = false;
            this.setState(state);
        }

        if (state.paused) {
            console.log("set timeout");
            this.toggleTimeout = window.setTimeout(continueFeed, 60000);
        }
    }

    registerChart() {
        this.socket.send(JSON.stringify({
            action: "register",
            config: this.state.config
        }));
    }

    unregisterChart() {
        this.socket.send(JSON.stringify({
            action: "unregister",
        }));
    }
}

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    render() {
        return (
            <BrowserRouter>
                <div style={{width: "100%"}}>
                    <Route path="/" component={Chart} />
                </div>
            </BrowserRouter>
        );
    }
}

const wrapper = document.getElementById("app");
wrapper ? ReactDOM.render(<App />, wrapper) : null;
