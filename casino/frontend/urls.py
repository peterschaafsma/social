from django.urls import path
from . import views


urlpatterns = [
    path('chart', views.chart),
    path('notify', views.notify),
    path('stock', views.stock),
    path('posts', views.posts),
    path('symbols', views.symbols),
]