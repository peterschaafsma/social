from api.consumers import Consumer
from api.models import Config
from django.shortcuts import redirect, render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from tools.rules import get_symbols, get_symbols_and_names
from core.posthandler import PostHandler
from urllib.parse import urlencode

import json


# Create your views here.
def chart(request):
    if request.method == 'GET':
        do_load_config = True
        for param in ('symbols', 'type', 'size'):
            if param in request.GET:
                do_load_config = False
                break

        if do_load_config:
            config = None
            try:
                config = Config.objects.get(tag='default')
            except:
                pass

            if config:
                params = json.loads(config.json)
                query_string = urlencode(params)

                url = '{}?{}'.format(request.path, query_string)

                return redirect(url)
        else:
            params = {}
            for param in ('symbols', 'type', 'size'):
                if param in request.GET:
                    params[param] = request.GET[param]

            config, created = Config.objects.get_or_create(tag='default')
            config.json = json.dumps(params)
            config.save()

        return render(request, 'frontend/chart.html')


@csrf_exempt
def notify(request):
    if request.method == 'GET':
        def process(config):
            # print("config", config)
            config = {**config}
            if 'filter' in config:
                config['filter'] = json.loads(config.get('filter') or '{}')

            return config

        config_data = {key: process(value['config']) for key, value in Consumer.CONNECTIONS.items()}

        return JsonResponse(config_data)

    if request.method == 'POST':
        body = json.loads(request.body.decode())

        for config_hash, data in body.items():
            Consumer.send(config_hash, data)

        return HttpResponse()


def stock(request):
    if request.method == 'GET':
        #symbols = sorted(map(str.upper, get_symbols()))

        rule_elements = sorted(get_symbols_and_names())

        return JsonResponse({"symbols": rule_elements, "rule_elements": rule_elements})

@csrf_exempt
def posts(request):
    if request.method == 'POST':
        body = json.loads(request.body.decode())

        # print("received post", body)

        PostHandler.process(body['source'], body['posts'])

        return HttpResponse()

def symbols(request):
    if request.method == 'GET':
        followed_symbols = set()
        for _, config in Consumer.CONNECTIONS.items():
            followed_symbols |= set(config['config'].get('symbols', []))

        return JsonResponse({"symbols": sorted(followed_symbols)})