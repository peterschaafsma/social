#! /usr/bin/env python

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import re
import sys
import time

url = sys.argv[1]

options = Options()
#chrome_options.add_argument("--disable-extensions")
#chrome_options.add_argument("--disable-gpu")
#chrome_options.add_argument("--no-sandbox") # linux only
options.add_argument("--headless")

caps = DesiredCapabilities.CHROME
caps['goog:loggingPrefs'] = {'performance': 'ALL'}

driver = webdriver.driver = webdriver.Chrome('/home/peter/Downloads/chromedriver', options=options, desired_capabilities=caps)
driver.get(url)
driver.execute_script("window.scrollTo(0, document.body.scrollHeight / 2)") 

time.sleep(1)

log = driver.get_log('performance')
#print(log)
tokens = set([m for m in re.findall(r'(?<=token=)\w+', str(log))])

if tokens:
  print(list(tokens)[0])
  
  driver.quit()
