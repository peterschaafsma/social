#! /usr/env/python
import re


input_file = '/home/peter/programming/stock/data/stock_data.txt'

def main():
    input_data = open(input_file).read()

    records = []
    cache = None
    for line in map(lambda x: x.split('\t'), input_data.splitlines()):
        if any(map(lambda x: len(x) == 0,line)) or len(line) < 5:
            if line == ['Euronext']:
                line = ['', '', '', 'Euronext']
            cache = line
        else:
            if cache:
                for i, string in enumerate(cache):
                    line[i] = (string + ' ' + line[i]).strip()
                cache = None

            name_clean = line[0].lower()
            name_clean = re.sub(r'[\.,]', '', name_clean).strip()
            name_clean = re.sub(r'[-]', ' ', name_clean).strip()
            while True:
                name_cleaner = re.sub(r' inc$| oyj$| sa$| ltd$| plc$| adp$| nv$| adr$| co$| sab$| de cv$', '', name_clean).strip()
                if name_cleaner == name_clean:
                    break
                name_clean = name_cleaner

            name_clean = re.sub(r'\W$|^\W', '', name_clean).strip()

            record = {
                'name': line[0],
                'name_clean': name_clean,
                'symbol': line[1],
                'exchange': line[2],
                'market': line[3],
                'isincode': line[4]
            }

            records.append(record)

            print(record['name'])
            print(record['name_clean'])
            print()

if __name__ == '__main__':
    main()