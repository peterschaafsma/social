import json
import pika

def enqueue(data, queue):
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.queue_declare(queue)
    channel.basic_publish(exchange='', routing_key=queue, body=json.dumps(data).encode('utf-8'))
    channel.close()

def dequeue(queue, callback):
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.queue_declare(queue)
    channel.basic_consume(queue=queue, auto_ack=True, on_message_callback=callback)
    channel.start_consuming()
