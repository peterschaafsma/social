def generate_rules():
    template = 'has:links -is:retweet -is:reply lang:en ({})'

    symbols = get_symbols()

    rules = []
    for i in range(0, len(symbols), 28):
        rules.append(template.format(' OR '.join(symbols[i:i + 28])))

    return {"tag_{}".format(i): {"value": rule, "tag": "tag_{}".format(i)} for i, rule in enumerate(rules)}

def get_symbols():
    stock_data = eval(open('/home/peter/programming/stock/data/stock_data.txt').read())

    symbols = list(zip(*stock_data))[0]

    return symbols

def get_symbols_and_names():
    stock_data = eval(open('/home/peter/programming/stock/data/stock_data.txt').read())

    items = []
    for item in stock_data:
        items.append('${}'.format(item[0]).upper())
        items.extend(item[2:])

    return items

